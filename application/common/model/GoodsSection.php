<?php

namespace app\common\model;

use think\Model;

class GoodsSection extends Model
{
    //
    public function getIsJinpingTextAttr($value,$data)
    {
        $text = ['否','是'];
        return $text[$data['is_jinping']];
    }
    public function getIsIndexTextAttr($value,$data)
    {
        $text = ['否','是'];
        return $text[$data['is_index']];
    }
    public function getUserTypeTextAttr($value,$data)
    {
        $text = ['不限','淘宝','天猫'];
        return $text[$data['user_type']];
    }
}
