<?php

namespace app\admin\controller\good;

use think\Controller;
use app\common\controller\Admin;

class Mama extends Admin
{
    protected $model;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('GoodsMamaCate');
    }
    /**
     * 选品库列表
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $list = $this->model
            ->order('favorites_id desc')
            ->paginate(input('limit',15));
            //$list->append(['status_text','pid_nickname']);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }
    /**
     * 导入
     */
    public function import()
    {
        $options = [
            'appkey' => config('site.tkcfg.tk_appkey'),
            'appsecret' => config('site.tkcfg.tk_appsecret')
        ];
        $tk = new \ddj\Tk($options);
        $re = $tk->favorites_get();
        if ($re['results']){
            //删除原来的
            db('GoodsMamaCate')->delete(true);
            $this->model->saveAll($re['results']['tbk_favorites'],false);
        }
        $this->success('操作成功');
    }
}
