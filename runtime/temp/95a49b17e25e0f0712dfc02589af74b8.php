<?php /*a:2:{s:83:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/collect/collect/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
  <ul class="layui-tab-title">
    <li class="layui-this">大淘客采集</li>
    <li>轻淘客采集</li>
    <li>好单库</li>
    <li>好品推</li>
    <li>淘客基地</li>
  </ul>
  <blockquote class="layui-elem-quote layui-tab-content" style="margin-top: 8px;">
    <div class="layui-tab-item layui-show" data-fm="dtk">
		<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
		<a href="javascript:;" class="layui-btn layui-btn-sm collect" data-url="<?php echo url('collect.dtk/qzlq'); ?>" data-title="全站领券采集[每页50条]" ><i class="fa fa-cloud-download" aria-hidden="true"></i> 全站领券采集</a>
		<a href="javascript:;" class="layui-btn layui-btn-sm collect" data-url="<?php echo url('collect.dtk/sspl'); ?>" data-title="实时跑量采集[每页100条]"><i class="fa fa-cloud-download" aria-hidden="true"></i> 实时跑量采集</a>
		<a href="javascript:;" class="layui-btn layui-btn-sm" id="dtk100" ><i class="fa fa-cloud-download" aria-hidden="true"></i> TOP100人气榜采集</a>
    </div>
    <div class="layui-tab-item">等待支持1</div>
    <div class="layui-tab-item">等待支持2</div>
    <div class="layui-tab-item">等待支持3</div>
    <div class="layui-tab-item">等待支持4</div>
    <hr/>
    <button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
				<i class="layui-icon">&#xe640;</i> 删除所选
			</button>
			<a href="javascript:;" class="layui-btn layui-btn-sm import checked" ><i class="layui-icon">&#xe654;</i>导入所选</a>
			<a href="javascript:;" class="layui-btn layui-btn-sm import all" ><i class="layui-icon">&#xe654;</i>导入全部</a>
			<button url="<?php echo url('del',['type'=>'all']); ?>" hide-data="true" lay-submit lay-filter="ajax-post"  class="layui-btn layui-btn-sm layui-btn-danger confirm"><i class="layui-icon">&#x1002;</i>清空采集库(所有)</button>

			<form class="layui-form search" action="" style="display: inline-block;float: right;" _lpchecked="1">
				<div class="layui-form-item" style="display: inline-block;margin:0;float: left;">
				  <div class="layui-inline">
				    <div class="layui-input-inline" style="width: 50px;">
				      <input type="number" name="price_min" placeholder="￥价" autocomplete="off" class="layui-input">
				    </div>
				    <div class="layui-form-mid">-</div>
				    <div class="layui-input-inline" style="width: 50px;">
				      <input type="number" name="price_max" placeholder="￥格" autocomplete="off" class="layui-input">
				    </div>
				  </div>
				  
				</div>
				<div class="layui-inline">
					<div class="layui-input-inline" style="width: 120px;">
						<select name="from" >
					        <option value="">采集库</option>
					        <option value="dtk">大淘客</option>
					        <option value="qtk">轻淘客</option>
					    </select>
					</div>
				</div>
				<div class="layui-inline">
					<div class="layui-input-inline" style="width: 120px;">
						<select name="cid" >
					        <option value="">分类</option>
					        <?php $_result=config('site.default_cate');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($key > '0'): ?>
					        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
					        <?php endif; ?>
					        <?php endforeach; endif; else: echo "" ;endif; ?>
					    </select>
					</div>
				</div>
				<div class="layui-inline">
					<div class="layui-input-inline">
						<input type="text" name="keyword" placeholder="商品ID/名称" autocomplete="off" class="layui-input">
					</div>
				</div>
				 <div class="layui-inline">
				 	<div class="layui-input-inline">
				 		<button class="layui-btn layui-btn-sm sbtn" lay-submit="" lay-filter="searchsub" id="search"><i class="layui-icon"></i> 搜索</button>
				 		<a href="javascript:;" class="layui-btn layui-btn-sm import searched"><i class="layui-icon">&#xe654;</i>导入搜索结果</a>
				 	</div>
				 </div>
			</form>
	</blockquote>
  
</div>

<table id="tb1" lay-filter="_tb1"></table>

<div id="import-form" style="display: none;">
	<form class="layui-form" action="">
	  <br/>
	  <div class="layui-form-item">
	    <label class="layui-form-label">选择版块</label>
	    <div class="layui-input-inline">
	      <select name="section_id" lay-verify="required">
	        <option value="0">自动识别</option>
	        <?php if(is_array($sections) || $sections instanceof \think\Collection || $sections instanceof \think\Paginator): $i = 0; $__LIST__ = $sections;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
	        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
	        <?php endforeach; endif; else: echo "" ;endif; ?>
	      </select>
	    </div>
	  </div>
	</form>
</div>
<div id="page" style="display: none;">
	<div class="layui-form-item">
	  <br/>
	   <div class="layui-inline">
	    <label class="layui-form-label">页码</label>
	    <div class="layui-input-inline" style="width: 100px;">
	      <input type="text" name="page_start"  autocomplete="off" class="layui-input" value="1">
	    </div>
	    <div class="layui-form-mid">-</div>
	    <div class="layui-input-inline" style="width: 100px;">
	      <input type="text" name="page_end"  autocomplete="off" class="layui-input" value="50">
	    </div>
	  </div>
	</div>
</div>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.id }}">
</script>
<script type="text/html" id="bar">
	  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.id }}" >删除</a>
</script>
<script type="text/html" id="pic">
	<img src="{{d.pic}}" width="20" height="20" class="show_img">
</script>
<script type="text/html" id="item_id">
	<span class="layui-text"><a href="https://item.taobao.com/item.htm?id={{d.item_id}}" target="_blank" >{{d.item_id}}</a></span>
</script>
<script>
	layui.use(['tool','element'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool,element = layui.element;
		tool.show_img();
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('index'); ?>',
			limit:50,
			limits:[20,50,100,200,500],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-215',
			cols:[[
				{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'商品ID',field:'item_id',width:150,templet:'#item_id'},
				{title:'分类',field:'cid_text',width:80},
				{title:'商品名称',field:'title',minWidth:300},
				{title:'图片',field:'pic',templet:'#pic',width:60},
				{title:'视频',field:'video_url',width:80},
				{title:'天猫淘宝',field:'is_tmall_text',width:100},
				{title:'售价',field:'price',width:80},
				{title:'优惠券',field:'quan_price',width:80},
				{title:'券后价',field:'price_after_quan',width:80},
				{title:'佣金比例',field:'rate',style:'color:red;',width:100},
				{title:'销量',field:'sales',width:80},
				{title:'结束时间',field:'quan_end_time',width:165},
				{title:'店铺',field:'shop_title',width:100},
				{title:'操作',fixed: 'right', width:80, align:'center', templet: '#bar'}

			]]
		});
		//搜索
		form.on('submit(searchsub)',function(data){
			var fields = $(data.form).serialize();
			tableobj.reload({
				where:data.field
				,page: {curr: 1 }
			});
			return false;
		})
		//导入
		$('.import').click(function(){
			var url = '<?php echo url('import'); ?>';
			var title = '导入全部';
			var ids='';
			var type = 'all';
			if ($(this).hasClass('checked')) {
				title = '导入所选';
				type = 'checked';
				var ids = $('.ids').serialize();
			}
			if ($(this).hasClass('searched')) {
				title = '导入搜索结果';
				type = 'searched';
				var ids = $('.search').serialize();
			}
			//页面层
			var fm = layer.open({
			  title:title
			  ,type: 1
			  //skin: 'layui-layer-rim', //加上边框
			  ,area: ['420px', '440px'] //宽高
			  ,content: $('#import-form')
			  ,btn: ['确定', '取消']
			  ,fixed: false
			  ,yes: function(index, layero){
			    var section_id = $('select[name=section_id]').val();
			    var load = layer.load(2);
				$.post(url,{ids:ids,section_id:section_id,type:type},function(ret){
					layer.msg(ret.msg);
					layer.close(load);
					layer.close(fm);
					tableobj.reload();
				})
			  }
			});
		});
		//top100
		$('#dtk100').click(function(){
			var url = '<?php echo url('collect.dtk/top100'); ?>'
			layer.confirm('确定采集？这个采集结果只有100件，请采完后导入到商品库', {
			  title:'温馨提示'
			}, function(index){
				var index = layer.load(2);
				$.post(url,function(ret){
					layer.close(index);
					if (ret.code==1) {
						tableobj.reload();
					}
					layer.msg(ret.msg);
				})
			});
		});
		//全站领券采集
		$('.collect').click(function(){
			var url = $(this).data(url);
			var fm = layer.open({
			  title:$(this).data('title')
			  ,type: 1
			  ,area: ['420px', '240px'] //宽高
			  ,content: $('#page')
			  ,btn: ['确定', '取消']
			  ,fixed: false
			  ,yes: function(index, layero){
			    var page_start = $('input[name=page_start]').val(),page_end = $('input[name=page_end]').val();
			    var load = layer.load(2);
			    $.get(url,{page_start:page_start,page_end:page_end},function(ret){
			    	layer.close(load);
					layer.close(fm);
			    	if (ret.code==2) {
			    		collect_page(url,ret.data.page_start,page_end);
			    	}else if(ret.code==0){
			    		layer.msg(ret.msg,{icon:2});
			    	}else{
			    		layer.msg(ret.msg,{icon:1},function(){
			    			location.reload();
			    		})
			    	}
			    });
			  }
			});
		});
		function collect_page(url,page_start,page_end){
			$.get(url,{page_start:page_start,page_end:page_end},function(ret){
				if (ret.code==2) {
					layer.msg(ret.msg, {
					  icon: 16
					  ,shade: 0.01
					  ,isOutAnim: false
					  ,anim:-1
					});
		    		collect_page(url,ret.data.page_start,ret.data.page_end);
		    	}else if(ret.code==0){
		    		layer.msg(ret.msg,{icon:2});
		    	}else{
		    		layer.msg(ret.msg,{icon:1},function(){
		    			location.reload();
		    		})
		    	}
			});
		}
	});
</script>

</html>