<?php /*a:2:{s:71:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/set/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
  <ul class="layui-tab-title">
    <li class="layui-this">支付配置</li>
    <li>短信配置</li>
    <li>淘客参数配置</li>
    <li>七牛云存储</li>
  </ul>
  <div class="layui-tab-content" style="height: 100px;">
    <div class="layui-tab-item layui-show">
       <fieldset class="layui-elem-field">
        <legend>支付宝 - 手机支付配置</legend>
        <div class="layui-field-box">
           <form class="layui-form" action="<?php echo url('edit'); ?>">
            <div class="layui-form-item">
              <label class="layui-form-label">app_id：</label>
              <div class="layui-input-inline">
                  <input type="text" name="config[alipay_cfg_wap][app_id]" required  lay-verify="required" placeholder="支付宝应用id" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['app_id']); ?>">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">商户私钥：</label>
              <div class="layui-input-block">
                  <input type="text" name="config[alipay_cfg_wap][merchant_private_key]" required  lay-verify="required" placeholder="请填写开发者私钥去头去尾去回车，一行字符串" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['merchant_private_key']); ?>">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">支付宝公钥：</label>
              <div class="layui-input-block">
                  <input type="text" name="config[alipay_cfg_wap][alipay_public_key]" required  lay-verify="required" placeholder="请填写支付宝公钥，一行字符串" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['alipay_public_key']); ?>">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">支付完成打开链接：</label>
              <div class="layui-input-block">
                  <input type="text" name="config[alipay_cfg_wap][return_url]" required  lay-verify="required" placeholder="文章链接" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['return_url']); ?>">
              </div>
            </div>


            <fieldset class="layui-elem-field">
              <legend>支付宝 - 提现配置(可以同上，也可以换其它账号)</legend>
              <div class="layui-field-box">
                  <div class="layui-form-item">
                    <label class="layui-form-label">app_id：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="config[alipay_cfg_wap][app_id_draw]" required  lay-verify="required" placeholder="支付宝应用id" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['app_id_draw']); ?>">
                    </div>
                  </div>
                  <div class="layui-form-item">
                    <label class="layui-form-label">商户私钥：</label>
                    <div class="layui-input-block">
                        <input type="text" name="config[alipay_cfg_wap][merchant_private_key_draw]" required  lay-verify="required" placeholder="请填写开发者私钥去头去尾去回车，一行字符串" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['merchant_private_key_draw']); ?>">
                    </div>
                  </div>
                  <div class="layui-form-item">
                    <label class="layui-form-label">支付宝公钥：</label>
                    <div class="layui-input-block">
                        <input type="text" name="config[alipay_cfg_wap][alipay_public_key_draw]" required  lay-verify="required" placeholder="请填写支付宝公钥，一行字符串" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alipay_cfg_wap']['alipay_public_key_draw']); ?>">
                    </div>
                  </div>
              </div>
            </fieldset>




            <div class="layui-form-item">
              <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
              </div>
            </div>


          </form>
        </div>
      </fieldset>
      
     </div>

    <div class="layui-tab-item">
      <blockquote class="layui-elem-quote">只支持阿里云短信，以下配置请在技术大神的监督下操作。</blockquote>
      <fieldset class="layui-elem-field">
        <legend>阿里云短信配置</legend>
        <div class="layui-field-box">
          <form class="layui-form" action="<?php echo url('edit'); ?>">
            <div class="layui-form-item">
              <label class="layui-form-label">Access Key ID：</label>
              <div class="layui-input-inline">
                  <input type="text" name="config[alisms_accessKeyId]" required  lay-verify="required" placeholder="请输入Access Key ID" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alisms_accessKeyId']); ?>">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">Access Key Secret：</label>
              <div class="layui-input-block" style="width:300px;">
                <input type="text" name="config[alisms_accessKeySecret]" required  lay-verify="required" placeholder="请输入Access Key Secret" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alisms_accessKeySecret']); ?>">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">签名设置：</label>
              <div class="layui-input-inline">
                <input type="text" name="config[alisms_sign_name]" required  lay-verify="required" placeholder="请输入短信签名" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alisms_sign_name']); ?>">
              </div>
            </div>
            <fieldset class="layui-elem-field layui-field-title">
            <legend>短信模板设置</legend>
            <div class="layui-field-box">
              <div class="layui-form-item">
                  <label class="layui-form-label">注册&登录模板CODE：</label>
                  <div class="layui-input-inline">
                    <input type="text" name="config[alisms_tpl_reglogin]" required  lay-verify="required" placeholder="请输入模板CODE" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alisms_tpl_reglogin']); ?>">
                  </div>
                  <div class="layui-form-mid layui-word-aux">内容：验证码${code}，您正在登录，若非本人操作，请勿泄露。</div>
              </div>
              <div class="layui-form-item">
                  <label class="layui-form-label">身份验证模板CODE：</label>
                  <div class="layui-input-inline">
                    <input type="text" name="config[alisms_tpl_identity]" required  lay-verify="required" placeholder="请输入模板CODE" autocomplete="off" class="layui-input" value="<?php echo htmlentities($config['alisms_tpl_identity']); ?>">
                  </div>
                  <div class="layui-form-mid layui-word-aux">内容：验证码${code}，您正在进行身份验证，打死不要告诉别人哦！</div>
              </div>
            </div>
          </fieldset>

            <div class="layui-form-item">
              <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
              </div>
            </div>
          </form>
        </div>
      </fieldset>
    </div>

    <div class="layui-tab-item">
      <form class="layui-form" action="<?php echo url('edit'); ?>">

        <div class="layui-field-box">
            <fieldset class="layui-elem-field">
              <legend>淘宝</legend>
              <div class="layui-field-box">
                <div class="layui-form-item">
                    <label class="layui-form-label">appkey：</label>
                    <div class="layui-input-inline">
                      <input type="text" name="config[tk_cfg][taobao][appkey]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['appkey']) && ($config['tk_cfg']['taobao']['appkey'] !== '')?$config['tk_cfg']['taobao']['appkey']:'')); ?>">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">appsecret：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][appsecret]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['appsecret']) && ($config['tk_cfg']['taobao']['appsecret'] !== '')?$config['tk_cfg']['taobao']['appsecret']:'')); ?>">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">账户id：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][id]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['id']) && ($config['tk_cfg']['taobao']['id'] !== '')?$config['tk_cfg']['taobao']['id']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">PID:例：mm_43224451_42520286_233494528中的第一位43224451</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">主pid：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][pid]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['pid']) && ($config['tk_cfg']['taobao']['pid'] !== '')?$config['tk_cfg']['taobao']['pid']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">PID:例：mm_43224451_42520286_233494528</div>
                </div>
                <hr/>
                <div class="layui-form-item">
                    <label class="layui-form-label">群主高级权限接口appkey：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][big_appkey]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['big_appkey']) && ($config['tk_cfg']['taobao']['big_appkey'] !== '')?$config['tk_cfg']['taobao']['big_appkey']:'')); ?>"">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">群主高级权限接口appsecret：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][big_appsecret]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['big_appsecret']) && ($config['tk_cfg']['taobao']['big_appsecret'] !== '')?$config['tk_cfg']['taobao']['big_appsecret']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux"><a href="javascript:;" data-url="http://www.apptimes.cn/oauth" class="shouquan layui-btn layui-btn-sm layui-btn-danger">去授权</a></div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">分享域名：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][taobao][share_domain]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['taobao']['share_domain']) && ($config['tk_cfg']['taobao']['share_domain'] !== '')?$config['tk_cfg']['taobao']['share_domain']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">发现分享被封，请及时切换</div>
                </div>
              </div>
            </fieldset>
            <fieldset class="layui-elem-field">
              <legend>京东</legend>
              <div class="layui-field-box">
                <div class="layui-form-item">
                    <label class="layui-form-label">AppKey：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][appkey]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['appkey']) && ($config['tk_cfg']['jd']['appkey'] !== '')?$config['tk_cfg']['jd']['appkey']:'')); ?>">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">AppSecret：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][appsecret]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['appsecret']) && ($config['tk_cfg']['jd']['appsecret'] !== '')?$config['tk_cfg']['jd']['appsecret']:'')); ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">http://dev.jd.com/isv/jos/appList.action</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">access_token：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][accesstoken]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['accesstoken']) && ($config['tk_cfg']['jd']['accesstoken'] !== '')?$config['tk_cfg']['jd']['accesstoken']:'')); ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux"><a href="javascript:;" data-url="<?php echo url('/index/jdauth'); ?>" class="shouquan layui-btn layui-btn-sm layui-btn-danger">授权获取</a> 回调地址：<?php echo config('site.domain'); ?>/index/jdauth/jd_notify</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">联盟id：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][unionid]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['unionid']) && ($config['tk_cfg']['jd']['unionid'] !== '')?$config['tk_cfg']['jd']['unionid']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">https://media.jd.com/user/myunion</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">主推广位id：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][pid]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['pid']) && ($config['tk_cfg']['jd']['pid'] !== '')?$config['tk_cfg']['jd']['pid']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">注意：不是PID</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">授权KEY：</label>
                    <div class="layui-input-inline" style="width: 800px;">
                      <input type="text" name="config[tk_cfg][jd][key]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['key']) && ($config['tk_cfg']['jd']['key'] !== '')?$config['tk_cfg']['jd']['key']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">https://media.jd.com/master/account/josApiAuthApply</div>
                </div>
                <hr/>
                <div class="layui-form-item">
                    <label class="layui-form-label">借用高级权限appkey：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][big_appkey]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['big_appkey']) && ($config['tk_cfg']['jd']['big_appkey'] !== '')?$config['tk_cfg']['jd']['big_appkey']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">借用高级权限appsecret：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][jd][big_appsecret]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['big_appsecret']) && ($config['tk_cfg']['jd']['big_appsecret'] !== '')?$config['tk_cfg']['jd']['big_appsecret']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">借用高级权限access_token：</label>
                    <div class="layui-input-inline" style="width: 400px;">
                      <input type="text" name="config[tk_cfg][jd][big_accesstoken]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['jd']['big_accesstoken']) && ($config['tk_cfg']['jd']['big_accesstoken'] !== '')?$config['tk_cfg']['jd']['big_accesstoken']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux"></div>
                </div>
              </div>
            </fieldset>
            <fieldset class="layui-elem-field">
              <legend>拼多多</legend>
              <div class="layui-field-box">
                <div class="layui-form-item">
                    <label class="layui-form-label">client_id：</label>
                    <div class="layui-input-inline" style="width: 400px;">
                      <input type="text" name="config[tk_cfg][pdd][client_id]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['pdd']['client_id']) && ($config['tk_cfg']['pdd']['client_id'] !== '')?$config['tk_cfg']['pdd']['client_id']:'')); ?>">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">client_secret：</label>
                    <div class="layui-input-inline" style="width: 400px;">
                      <input type="text" name="config[tk_cfg][pdd][client_secret]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['pdd']['client_secret']) && ($config['tk_cfg']['pdd']['client_secret'] !== '')?$config['tk_cfg']['pdd']['client_secret']:'')); ?>">
                    </div>
                    <div class="layui-form-mid layui-word-aux">http://open.pinduoduo.com/#/application/info?id=2802</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">主pid：</label>
                    <div class="layui-input-inline" style="width: 300px;">
                      <input type="text" name="config[tk_cfg][pdd][pid]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['tk_cfg']['pdd']['pid']) && ($config['tk_cfg']['pdd']['pid'] !== '')?$config['tk_cfg']['pdd']['pid']:'')); ?>"">
                    </div>
                    <div class="layui-form-mid layui-word-aux">http://jinbao.pinduoduo.com/manage/pid-manage</div>
                </div>
              </div>
            </fieldset>
          <div class="layui-form-item">
            <div class="layui-input-block">
              <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
              <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
          </div>
          
        </div>
      </form>
    </div>
    <!-- 七牛 -->
    <div class="layui-tab-item">
      <form class="layui-form" action="<?php echo url('edit'); ?>">
      <div class="layui-form-item">
        <label class="layui-form-label">开启：</label>
        <div class="layui-input-inline">
          <select name="config[qiniu_cfg][is_open]" lay-verify="required">
            <option value="">请选择</option>
            <option value="local">本地存储</option>
            <option value="qiniu">我要用七牛</option>
          </select> 
        </div>
      </div>
      <div class="layui-form-item">
          <label class="layui-form-label">accessKey：</label>
          <div class="layui-input-inline" style="width: 500px;">
            <input type="text" name="config[qiniu_cfg][access_key]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['qiniu_cfg']['access_key']) && ($config['qiniu_cfg']['access_key'] !== '')?$config['qiniu_cfg']['access_key']:'')); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">AK</div>
      </div>
      <div class="layui-form-item">
          <label class="layui-form-label">secretKey：</label>
          <div class="layui-input-inline" style="width: 500px;">
            <input type="text" name="config[qiniu_cfg][secret_key]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['qiniu_cfg']['secret_key']) && ($config['qiniu_cfg']['secret_key'] !== '')?$config['qiniu_cfg']['secret_key']:'')); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">SK</div>
      </div>
      <div class="layui-form-item">
          <label class="layui-form-label">bucket：</label>
          <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="config[qiniu_cfg][bucket]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['qiniu_cfg']['bucket']) && ($config['qiniu_cfg']['bucket'] !== '')?$config['qiniu_cfg']['bucket']:'')); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">请保证存在！</div>
      </div>
      <div class="layui-form-item">
          <label class="layui-form-label">访问域名：</label>
          <div class="layui-input-inline" style="width: 500px;">
            <input type="text" name="config[qiniu_cfg][domain]" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo htmlentities((isset($config['qiniu_cfg']['domain']) && ($config['qiniu_cfg']['domain'] !== '')?$config['qiniu_cfg']['domain']:'')); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">访问域名！</div>
      </div>
      <div class="layui-form-item">
        <div class="layui-input-block">
          <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
          <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
      </div>
      </form>
    </div>


  </div>
</div> 


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool','element'],function(){
  var $ = layui.jquery,layer = layui.layer,form = layui.form,tool = layui.tool,element = layui.element;
  $('.shouquan').click(function(){
    layer.open({
      type: 2,
      title: '高佣授权',
      shadeClose: false,
      shade: false,
      area: ['80%', '80%'],
      content: $(this).data('url') //iframe的url
    }); 
  });
  tool.setValue('config[qiniu_cfg][is_open]','<?php echo htmlentities($config['qiniu_cfg']['is_open']); ?>')
});
</script>

</html>