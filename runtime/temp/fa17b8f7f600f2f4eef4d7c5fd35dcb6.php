<?php /*a:2:{s:77:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/user/draw/index.html";i:1547509564;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<p>今日申请：￥<?php echo htmlentities($today_total); ?>&nbsp;&nbsp;&nbsp;&nbsp;累积打款:￥<?php echo htmlentities($all_total); ?></p>
	<hr>
	<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button>
	<form class="layui-form search" action="" style="display: inline-block;float: right;" _lpchecked="1">
		<div class="layui-input-inline" style="width: 300px;">
			<input type="text" name="date" class="layui-input" id="date" value="<?php echo date('Y-m-d 00:00:00',time()); ?> ~ <?php echo date('Y-m-d 23:59:59',time()); ?>">
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 80px;">
				<select name="status" >
			        <option value="">状态</option>
			        <option value="0">待审核</option>
			        <option value="1">提现成功</option>
			        <option value="2">驳回</option>
			        <option value="3">标记成功</option>
			    </select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 100px;">
				<input type="text" name="uid" placeholder="ID" autocomplete="off" class="layui-input">
			</div>
		</div>
		 <div class="layui-inline">
		 	<div class="layui-input-inline">
		 		<button class="layui-btn layui-btn-sm sbtn" lay-submit="" lay-filter="searchsub" id="search"><i class="layui-icon"></i> 搜索</button>
		 		<button class="layui-btn layui-btn-sm layui-btn-danger"  id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 导出</button>
		 	</div>
		 </div>
	</form>
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.id }}">
</script>
<script type="text/html" id="bar">
	<div class="layui-btn-group">
	  {{# if(d.draw_type==0){  }}
	  <a class="layui-btn layui-btn-xs dopay" data-url="<?php echo url('wx_pay'); ?>?id={{ d.id }}" >微信打款</a>
	  {{# }else{ }}
	  <a class="layui-btn layui-btn-xs dopay" data-url="<?php echo url('alipay'); ?>?id={{ d.id }}" >支付宝打款</a>
	  {{#  } }}
	  <a class="layui-btn layui-btn-xs dopay" data-url="<?php echo url('mark_pay'); ?>?id={{ d.id }}" >标记已打</a>
	  <a class="layui-btn layui-btn-xs layui-btn-danger dopay" data-url="<?php echo url('down_pay'); ?>?id={{ d.id }}" >驳回</a>
	  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.id }}" >删除</a>
	</div>
</script>
<script type="text/html" id="uid">
	<span class="layui-text"><a href="javascript:;" class="show_userinfo"  data-title="【{{d.user_nickname}}】的用户信息" data-url="<?php echo url('user.index/info'); ?>?id={{d.uid}}" >[{{d.uid}}]{{d.user_nickname}}</a></span>
</script>
<script>
	layui.use(['tool','laydate'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool,laydate=layui.laydate;
		tool.show_userinfo();
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('index'); ?>',
			limit:15,
			limits:[10,15,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-145',
			cols:[[
				{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'用户',field:'uid',templet:'#uid'},
				{title:'实际打款',field:'money',style:'color:#ff435b;'},
				{title:'提现金额',field:'money_rel'},
				{title:'真实姓名',field:'real_name'},
				{title:'支付宝',field:'alipay_account'},
				{title:'手机',field:'mobile'},
				{title:'提现方式',field:'draw_type_text',width:90},
				{title:'申请时间',field:'create_time'},
				{title:'处理状态',field:'status_text',width:100},
				{title:'处理时间',field:'update_time'},
				{title:'操作',fixed: 'right', align:'center', templet: '#bar',width:250}

			]]
		});
		//微信打款
		$(document).on('click','.dopay',function(){
			var url = $(this).data('url');
			layer.confirm('想好了，不后悔？', {
			  //btn: ['重要','奇葩'] //按钮
			  title:'非温馨提示'
			}, function(index){
				var index = layer.load(2);
				$.post(url,function(ret){
					layer.close(index);
					if (ret.code==1) {
						tableobj.reload();
					}
					layer.msg(ret.msg);
				})
			});
		});
		//搜索
		form.on('submit(searchsub)',function(data){
			var fields = $(data.form).serialize();
			tableobj.reload({
				where:data.field
				,page: {curr: 1 }
			});
			return false;
		})
		//日期时间范围
		laydate.render({
		  elem: '#date'
		  ,type: 'datetime'
		  ,range: '~'
		  ,min:'2018-10-01 00:00:00'
		  ,max:'<?php echo date('Y-m-d 23:59:59',time()); ?>'
		});
		//export
		$('#export').click(function(){
			var url = '<?php echo url('export',['type'=>'draw']); ?>?';
			var ids = $('.ids:checked').serialize();
			var form = $('form').serialize();
			location.href = url+'&'+form+'&'+ids;

			return false;
		});
	});
</script>

</html>